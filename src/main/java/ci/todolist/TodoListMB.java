package ci.todolist;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

@ManagedBean
public class TodoListMB {

    @Inject
    private TodoItemRepository repository;

    @PostConstruct
    public void initialize() {
        if (repository.isEmpty()) {
            repository.create("Aprender TDD :)");
            repository.create("Aprender JPA o/");
            repository.create("Aprender EJB ;)");
            repository.create("Aprender JSF \\o/");
        }
    }

    public Long getTodosCount() {
        return repository.count();
    }

    public List<TodoItem> getTodos() {
        return repository.getAll();
    }

}
